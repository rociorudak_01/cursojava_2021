package modulo1;
import java.util.Scanner;

public class Tp2ejer12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		
		System.out.println("Ingrese un n�mero hasta el 36 y entero");
		int n = scan.nextInt();
		
		if (n >= 1 && n <= 12)
			System.out.println("Pertenece a la primer docena.");
		else if (n >= 12 && n <= 24)
			System.out.println("Pertenece a la segunda docena.");
		else if (n >= 24 && n <= 36)
			System.out.println("Pertenece a la tercer docena.");
		else if (n < 1 || n > 36 )
			System.out.println("No pertenece a ninguna docena. Vuelva a intentar");
		
		scan=null;
	}

	}

