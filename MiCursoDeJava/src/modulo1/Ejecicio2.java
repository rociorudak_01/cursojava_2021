package modulo1;

public class Ejecicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
         System.out.println("Tecla de escape \t\t significado");
         System.out.println("\\n \t\t\t\t significa nueva linea");
         System.out.println("\\t \t\t\t\t significa un tab de espacio");
         System.out.println("\\\" \t\t\t\t es para poner \" dentro del texto, por ejemplo \"Belencita\"");
         System.out.println("\\\\ \t\t\t\t se utiliza para escribir la \\ dentro del texto, por ejemplo \\algo\\");
       System.out.println("\\\' \t\t\t\t se utiliza para las comillas simples para escribir, por ejemplo \'Princesita\'");
	}

}
